<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/admin-dashboard', 'Admin\LoggedInController@index')->name('admin.dashboard');

//Route::get('home', 'HomeController@index')->name('home');

//Auth::routes();

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('log-out', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('register-complete', 'Auth\RegisterController@registerComplete')->name('register.complete');
Route::post('register', 'Auth\RegisterController@register')->name('register.handle');

//Check
Route::post('check-sms', 'Auth\CheckController@checkSms')->name('check.sms');
Route::get('email-information', 'Auth\CheckController@emailInfo')->name('email.info');
Route::get('email-check/{hash}', 'Auth\CheckController@checkEmail')->name('check.email');
Route::get('login/linkedin-callback', 'Auth\CheckController@linkedInCallback');
Route::get('login/linkedin', 'Auth\CheckController@linkedInLogin')->name('check.linkedin');

// Password Reset Routes...
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::get('user-type', 'Site\SiteController@chooser')->name('register.chooser');
Route::get('faq', 'Site\SiteController@faq')->name('faq');

//Route::get('user/check', 'User\UserIdentifier@check');

Route::namespace('User')
    ->prefix('company')
    ->middleware(['auth', 'user'])
    ->group(function () {
        Route::resource('inside', 'LoggedInController');
        Route::get('add-by-proxy','LoggedInController@addByProxy')->name('add-by-proxy');
        Route::get('profile', 'LoggedInController@profile')->name('profile');
        Route::get('show-contract/{path}', 'LoggedInController@showContract')->name('show-contract');
        Route::get('search', 'LoggedInController@search')->name('search-get');
        Route::get('watermark/{case_no}', 'LoggedInController@watermark')->name('watermark');
        Route::post('update', 'LoggedInController@updateProfile')->name('profile-update');
        Route::post('search', 'LoggedInController@doSearch')->name('search-post');
        Route::post('case-status', 'LoggedInController@caseStatus')->name('case-status');
        Route::post('save-inform', 'LoggedInController@saveInform')->name('save-inform');
        Route::post('save-possible-seller', 'LoggedInController@savePossibleSeller')->name('save-possible-seller');
    });

/*Route::namespace('User')
    ->prefix('user')
    ->middleware(['auth', 'buyer'])//buyer middleware Kernel.php içinde
    ->group(function () {
        Route::resource('buyers', 'BuyerController');
        Route::get('search-firm', 'BuyerController@search');
    });*/



Route::get('/', function () {
    return view('user.home');
})->name('home');
