<?php

return [
    'mode'         => 'utf-8',
    'format'       => 'A4',
    'author'       => 'Para Onay',
    'subject'      => '',
    'keywords'     => '',
    'creator'      => 'Para Onay',
    'display_mode' => 'fullpage',
    'tempDir'      => base_path('../temp/'),
    'font_path'    => base_path('storage/fonts/'),
    'font_data'    => [
        'roboto' => [
            'R'  => 'Roboto-Regular.ttf',    // regular font
            'B'  => 'Roboto-Bold.ttf',       // optional: bold font
            'I'  => 'Roboto-Italic.ttf',     // optional: italic font
            'BI' => 'Roboto-BoldItalic.ttf' // optional: bold-italic font
        ]
        // ...add as many as you want.
    ]
];
