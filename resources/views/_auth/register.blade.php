@extends('layouts.auth')
<style>
    /* Latest compiled and minified CSS included as External Resource*/

    /* Optional theme */

    /*@import url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css');*/
    body {
        margin-top:30px;
    }
    .stepwizard-step p {
        margin-top: 0px;
        color:#666;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        /*opacity: 1 !important;
        filter: alpha(opacity=100) !important;*/
    }
    .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
        opacity:1 !important;
        color:#bbb;
    }
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content:" ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-index: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
</style>
@section('content')
<div class="container">
    <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-3">
                    <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                    <p><small>Şirket Türü</small></p>
                </div>
                <div class="stepwizard-step col-xs-3">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Şirket Bilgileri</small></p>
                </div>
            </div>
        </div>

        <form role="form">
            <div class="panel panel-primary setup-content" id="step-1">
                <div class="panel-heading">
                    <h3 class="panel-title">Şirket Türü</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label">First Name</label>
                        <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Last Name</label>
                        <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" />
                    </div>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>

            <div class="panel panel-primary setup-content" id="step-2">
                <div class="panel-heading">
                    <h3 class="panel-title">Destination</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        @captcha('tr')
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tc_no') ? ' has-error' : '' }}">
                            <label for="tc_no" class="col-md-4 control-label">TC Kimlik No</label>

                            <div class="col-md-6">
                                <input id="tc_no" type="text" class="form-control" name="tc_no" value="{{ old('tc_no') }}" required>

                                @if ($errors->has('tc_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tc_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title_of_company') ? ' has-error' : '' }}">
                            <label for="title_of_company" class="col-md-4 control-label">Şirket Ünvanı</label>

                            <div class="col-md-6">
                                <input id="title_of_company" type="text" class="form-control" name="title_of_company" value="{{ old('title_of_company') }}" required>

                                @if ($errors->has('title_of_company'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_of_company') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('company_tax_no') ? ' has-error' : '' }}">
                            <label for="company_tax_no" class="col-md-4 control-label">Vergi Numarası</label>

                            <div class="col-md-6">
                                <input id="company_tax_no" type="text" class="form-control" name="company_tax_no" value="{{ old('company_tax_no') }}" required>

                                @if ($errors->has('company_tax_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_tax_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tax_office_name') ? ' has-error' : '' }}">
                            <label for="tax_office_name" class="col-md-4 control-label">Vergi Dairesi</label>

                            <div class="col-md-6">
                                <input id="tax_office_name" type="text" class="form-control" name="tax_office_name" value="{{ old('tax_office_name') }}" required>

                                @if ($errors->has('tax_office_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tax_office_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('gsm') ? ' has-error' : '' }}">
                            <label for="gsm" class="col-md-4 control-label">Cep Telefonu</label>

                            <div class="col-md-6">
                                <input id="gsm" type="text" class="form-control" name="gsm" value="{{ old('gsm') }}" required>

                                @if ($errors->has('gsm'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gsm') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                            <label for="city_id" class="col-md-4 control-label">Şehir</label>

                            <div class="col-md-6">
                                <input id="city_id" type="text" class="form-control" name="city_id" value="{{ old('city_id') }}" required>

                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="want_connection" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                <input type="checkbox" name="want_connection" id="want_connection" value="0"><label for="want_connection"><span></span> <p>ALICININ DOĞRUDAN İLETİŞİME GEÇMESİNİ İSTEMİYORUM. İŞLEM MUTABAKATINA KADAR İLETİŞİM VE BİLGİLERİMİN PAYLAŞILMASINI İSTEMİYORUM. PLATFORM ÜZERİNDEN DOSYANIN TEYİDİNİ İSTİYORUM</p></label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Kayıt Ol
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>


        </form>
    </div>
    {{--<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        @captcha('tr')
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tc_no') ? ' has-error' : '' }}">
                            <label for="tc_no" class="col-md-4 control-label">TC Kimlik No</label>

                            <div class="col-md-6">
                                <input id="tc_no" type="text" class="form-control" name="tc_no" value="{{ old('tc_no') }}" required>

                                @if ($errors->has('tc_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tc_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title_of_company') ? ' has-error' : '' }}">
                            <label for="title_of_company" class="col-md-4 control-label">Şirket Ünvanı</label>

                            <div class="col-md-6">
                                <input id="title_of_company" type="text" class="form-control" name="title_of_company" value="{{ old('title_of_company') }}" required>

                                @if ($errors->has('title_of_company'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_of_company') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('company_tax_no') ? ' has-error' : '' }}">
                            <label for="company_tax_no" class="col-md-4 control-label">Vergi Numarası</label>

                            <div class="col-md-6">
                                <input id="company_tax_no" type="text" class="form-control" name="company_tax_no" value="{{ old('company_tax_no') }}" required>

                                @if ($errors->has('company_tax_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_tax_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tax_office_name') ? ' has-error' : '' }}">
                            <label for="tax_office_name" class="col-md-4 control-label">Vergi Dairesi</label>

                            <div class="col-md-6">
                                <input id="tax_office_name" type="text" class="form-control" name="tax_office_name" value="{{ old('tax_office_name') }}" required>

                                @if ($errors->has('tax_office_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tax_office_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('gsm') ? ' has-error' : '' }}">
                            <label for="gsm" class="col-md-4 control-label">Cep Telefonu</label>

                            <div class="col-md-6">
                                <input id="gsm" type="text" class="form-control" name="gsm" value="{{ old('gsm') }}" required>

                                @if ($errors->has('gsm'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gsm') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                            <label for="city_id" class="col-md-4 control-label">Şehir</label>

                            <div class="col-md-6">
                                <input id="city_id" type="text" class="form-control" name="city_id" value="{{ old('city_id') }}" required>

                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="want_connection" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                <input type="checkbox" name="want_connection" id="want_connection" value="0"><label for="want_connection"><span></span> <p>ALICININ DOĞRUDAN İLETİŞİME GEÇMESİNİ İSTEMİYORUM. İŞLEM MUTABAKATINA KADAR İLETİŞİM VE BİLGİLERİMİN PAYLAŞILMASINI İSTEMİYORUM. PLATFORM ÜZERİNDEN DOSYANIN TEYİDİNİ İSTİYORUM</p></label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Kayıt Ol
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}
</div>
@endsection
