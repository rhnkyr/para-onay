@extends('layouts.user-main')


@section('content')
    <style>
        .btn-linkedin {
            background: #0E76A8;
            border-radius: 0;
            color: #fff;
            border-width: 1px;
            border-style: solid;
            border-color: #084461;
        }
        .btn-linkedin:link, .btn-linkedin:visited {
            color: #fff;
        }
        .btn-linkedin:active, .btn-linkedin:hover {
            background: #084461;
            color: #fff;
        }
    </style>
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-envelope font-green"></i>
                            <span class="caption-subject font-green bold uppercase">{{__('text.login')}}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="alert alert-info text-center">
                            <strong>{{__('text.info')}} </strong>{{__('text.sms_and_email_info')}}
                        </div>
                        {!! Form::open(['route' => 'login', 'class'=>'form-horizontal', 'id'=>'login']) !!}
                        @captcha('tr')
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{__('text.email')}}</label>

                            <div class="col-md-6">
                                {!! Form::text('email', old('email'), ['class' => 'form-control', 'required', 'autofocus', 'type'=>'email']); !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{__('text.password')}}</label>

                            <div class="col-md-6">
                                {!! Form::password('password',['class' => 'form-control', 'required']); !!}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{__('text.remember_me')}}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{__('text.login')}}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{__('text.forget_password')}}
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        {{--<div class="row">
                        <div class="center-block text-center">
                            <a href="{{route('check.linkedin')}}" title="Linkedin İle Giriş" class="btn btn-linkedin btn-lg"><i class="fa fa-linkedin fa-fw"></i> Linkedin İle Giriş</a>
                        </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
