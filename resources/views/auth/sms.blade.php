@extends('layouts.user-main')


@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bell font-green"></i>
                            <span class="caption-subject font-green bold uppercase">{{__('text.sms_approve')}} ({{$user->sms}})</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-group">
                            {!! Form::open(['route' => 'check.sms', 'class'=>'form-horizontal']) !!}
                            @captcha('tr')
                            <div class="form-group{{ $errors->has('sms') ? ' has-error' : '' }}">
                                <div class="col-md-6 col-md-offset-3">
                                    @if($errors->any())
                                    <div class="alert alert-danger">
                                        <strong>{{__('text.error')}} </strong>{{__('text.sms_error')}}
                                    </div>
                                    @endif
                                    {!! Form::hidden('uid',$user->id); !!}
                                    {!! Form::text('sms', null, ['class' => 'form-control', 'required', 'autofocus', 'maxlength'=> 6, 'placeholder'=> __('text.sms_place_holder')]); !!}
                                    @if ($errors->has('sms'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('sms') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{__('text.control_button')}}
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection