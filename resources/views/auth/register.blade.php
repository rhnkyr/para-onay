@extends('layouts.user-main')

@section('page-level-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-user font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Şirket Bilgileri</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel" id="step-2">
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" action="{{ route('register.handle') }}">
                                    {{ csrf_field() }}
                                    @captcha('tr')
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="proxy" class="col-md-4 control-label">Temsilci</label>
                                        <div class="col-md-6">
                                            <select id="proxy"
                                                    name="proxy"
                                                    class="form-control select2 select2-hidden-accessible"
                                                    tabindex="-1"
                                                    aria-hidden="true">
                                                <option value="1">Asıl</option>
                                                <option value="2">Avukat - Hukuk Bürosu</option>
                                                <option value="3">Muhasebe Ofisi</option>
                                                <option value="4">Danışman</option>
                                                <option value="5">Tahsilat Hizmetleri İşletmesi</option>
                                                <option value="6">Diğer</option>
                                            </select>
                                            <span class="help-block">İşletme dışından temsil ediyorsanız lütfen tanımlamalardan birini seçiniz</span>
                                            @if ($errors->has('proxy'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('proxy') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Adınız</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name"
                                                   value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                        <label for="surname" class="col-md-4 control-label">Soyadınız </label>

                                        <div class="col-md-6">
                                            <input id="surname" type="text" class="form-control" name="surname"
                                                   value="{{ old('surname') }}" required autofocus>

                                            @if ($errors->has('surname'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('surname') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('birth_year') ? ' has-error' : '' }}">
                                        <label for="birth_year" class="col-md-4 control-label">Doğum
                                            Yılınız </label>

                                        <div class="col-md-6">
                                            <input id="birth_year" type="text" class="form-control"
                                                   name="birth_year"
                                                   value="{{ old('birth_year') }}" required autofocus placeholder="Örn : 1975">

                                            @if ($errors->has('birth_year'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('birth_year') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail Adresi</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('tc_no') ? ' has-error' : '' }}">
                                        <label for="tc_no" class="col-md-4 control-label">TC Kimlik No</label>

                                        <div class="col-md-6">
                                            <input id="tc_no" type="text" class="form-control" name="tc_no"
                                                   value="{{ old('tc_no') }}" required>

                                            @if ($errors->has('tc_no'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tc_no') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('title_of_company') ? ' has-error' : '' }}">
                                        <label for="title_of_company" class="col-md-4 control-label">Şirket
                                            Ünvanı</label>

                                        <div class="col-md-6">
                                            <input id="title_of_company" type="text" class="form-control"
                                                   name="title_of_company" value="{{ old('title_of_company') }}"
                                                   required>

                                            @if ($errors->has('title_of_company'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('title_of_company') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('company_tax_no') ? ' has-error' : '' }}">
                                        <label for="company_tax_no" class="col-md-4 control-label">Vergi
                                            Numarası</label>

                                        <div class="col-md-6">
                                            <input id="company_tax_no" type="text" class="form-control"
                                                   name="company_tax_no" value="{{ old('company_tax_no') }}"
                                                   required>

                                            @if ($errors->has('company_tax_no'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('company_tax_no') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('tax_office_name') ? ' has-error' : '' }}">
                                        <label for="tax_office_name" class="col-md-4 control-label">Vergi
                                            Dairesi</label>

                                        <div class="col-md-6">
                                            {!! Form::select('tax_office_name',
                                            $tax_offices,
                                            null,
                                            ['class' => 'form-control select2', 'required']);
                                            !!}

                                            @if ($errors->has('tax_office_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_office_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('gsm') ? ' has-error' : '' }}">
                                        <label for="gsm" class="col-md-4 control-label">Cep Telefonu</label>

                                        <div class="col-md-6">
                                            <input id="gsm" type="text" class="form-control" name="gsm"
                                                   value="{{ old('gsm') }}" required>

                                            @if ($errors->has('gsm'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('gsm') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="want_connection" class="col-md-4 control-label"></label>

                                        <div class="col-md-6">
                                            <input type="checkbox" name="want_connection" id="want_connection"
                                                   value="0"><label for="want_connection"><span></span>
                                                <p>ALICININ DOĞRUDAN İLETİŞİME GEÇMESİNİ İSTEMİYORUM. İŞLEM
                                                    MUTABAKATINA KADAR İLETİŞİM VE BİLGİLERİMİN PAYLAŞILMASINI
                                                    İSTEMİYORUM. PLATFORM ÜZERİNDEN DOSYANIN TEYİDİNİ İSTİYORUM</p>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Kayıt Ol
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-vendors-js')
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
@endsection

@section('page-level-js')
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $('input[name="company_tax_no"]').mask('9999999999', {placeholder: ' '});
            $('#gsm').mask('0(999) 999-9999');
            $('#birth_year').mask('9999', {placeholder: ' '});
        });
    </script>
@endsection