@extends('layouts.user-main')


@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-envelope font-green"></i>
                            <span class="caption-subject font-green bold uppercase">{{__('text.email_approve')}}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="alert alert-info">
                            <strong>{{__('text.info')}} </strong>{{__('text.email_info')}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection