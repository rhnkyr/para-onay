@extends('layouts.user-main')

@section('content')
    <style>
        .nf {
            float: none !important;
        }
    </style>
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="row text-center margin-bottom-20">
                    <h1>LÜTFEN YAPMAK İSTEDİĞİNİZ İŞLEMİ SEÇİNİZ</h1>
                </div>
                <div class="row widget-row text-center margin-bottom-20">
                    <div class="col-md-4">
                        <!-- BEGIN WIDGET THUMB -->
                        <a href="{{route('register', ['type' => 1])}}">
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                                <h4 class="widget-thumb-heading">BORÇLARIMI İNDİRİMLİ ÖDEMEK İSTİYORUM</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-green icon-arrow-down nf"></i>
                                </div>
                            </div>
                        </a>
                        <!-- END WIDGET THUMB -->
                    </div>
                    <div class="col-md-4">
                        <!-- BEGIN WIDGET THUMB -->
                        <a href="{{route('register', ['type' => 2])}}">
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                                <h4 class="widget-thumb-heading">ALACAĞIMI TAHSİL ETMEK İSTİYORUM</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red icon-arrow-right nf"></i>
                                </div>
                            </div>
                        </a>
                        <!-- END WIDGET THUMB -->
                    </div>
                    <div class="col-md-4">
                        <!-- BEGIN WIDGET THUMB -->
                        <a href="{{route('register', ['type' => 2])}}">
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                                <h4 class="widget-thumb-heading">ALACAK RİSKLERİNDEN KORUNMAK İSTİYORUM</h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-purple icon-shield nf"></i>
                                </div>
                            </div>
                        </a>
                        <!-- END WIDGET THUMB -->
                    </div>
                </div>
                <div class="row text-center">
                    <span class="alert alert-warning">DİKKAT! SİSTEM BANKALARA OLAN SORUNLU BORÇLARINIZIN İNDİRİMLİ ÖDENMESİ HAKKINDA DEĞİLDİR.</span>
                </div>
            </div>
        </div>
    </div>
@endsection