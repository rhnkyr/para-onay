<div class="page-header-menu">
    <div class="container-fluid">
        <!-- BEGIN MEGA MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
        <div class="hor-menu hor-menu-light  ">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{route('home')}}"> Ana Sayfa
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('register')}}"> Kayıt Ol
                        <span class="arrow"></span>
                    </a>
                </li>
                {{--<li>
                    <a href="{{route('faq')}}"> SSS
                        <span class="arrow"></span>
                    </a>
                </li>--}}
                <li>
                    <a href="{{route('login')}}"> Giriş Yap
                        <span class="arrow"></span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- END MEGA MENU -->
    </div>
</div>