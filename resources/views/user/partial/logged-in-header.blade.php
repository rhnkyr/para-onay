<div class="page-header-menu">
    <div class="container-fluid">
        <!-- BEGIN MEGA MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
        <div class="hor-menu hor-menu-light  ">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{route('home')}}" >{{__('labels.inside.home')}}
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('inside.index')}}" >{{__('labels.inside.dashboard')}}
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('inside.create')}}" >{{__('labels.inside.case_title_menu')}}
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('search-get')}}"> {{__('labels.inside.search_company')}}
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('add-by-proxy')}}"> {{__('labels.inside.proxy_case_title')}}
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('profile')}}"> {{__('labels.inside.profile')}}
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="#"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        {{__('text.user.logout')}}
                    </a>
                    {!! Form::open(['route'=>'logout','id'=>'logout-form', 'style'=>'display:none;']) !!}{!! Form::close() !!}
                </li>
            </ul>
        </div>
        <!-- END MEGA MENU -->
    </div>
</div>