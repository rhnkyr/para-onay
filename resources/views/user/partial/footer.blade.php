<div class="page-wrapper-row">
    <div class="page-wrapper-bottom">
        <!-- BEGIN FOOTER -->
        <div class="page-prefooter">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                        <h2>Hakkında</h2>
                        <p> Para Onay </p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                        <h2>Email Kayıt</h2>
                        <div class="subscribe-form">
                            <form action="javascript:;">
                                <div class="input-group">
                                    <input type="text" placeholder="mail@email.com" class="form-control">
                                    <span class="input-group-btn">
                                                    <button class="btn" type="submit">Kaydet</button>
                                                </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-block col-md-offset-3">
                        <h2>İletişim</h2>
                        <address class="margin-bottom-40"> Telefon: 800 123 3456
                            <br> Mail:
                            <a href="mailto:info@metronic.com">info@paraonay.com</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PRE-FOOTER -->
    </div>
</div>