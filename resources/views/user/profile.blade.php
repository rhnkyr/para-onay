@extends('layouts.user-main')
@section('top-line')
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{__('labels.inside.profile')}}</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection
@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-user font-green"></i>
                            <span class="caption-subject font-green bold uppercase">{{__('labels.inside.profile')}}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel" id="step-2">
                            <div class="panel-body">
                                    {!! Form::model($user,['route' => ['profile-update'], 'class'=>'form-horizontal', 'method'=>'POST']) !!}
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                    <div class="form-group">
                                        <label for="proxy" class="col-md-4 control-label">Temsilci</label>
                                        <div class="col-md-6">
                                            <select id="proxy"
                                                    name="proxy"
                                                    class="form-control select2 select2-hidden-accessible"
                                                    tabindex="-1"
                                                    aria-hidden="true" disabled>
                                                <option value="1" @if($user->proxy === 1) selected @endif>Asıl</option>
                                                <option value="2" @if($user->proxy === 2) selected @endif>Avukat - Hukuk
                                                    Bürosu
                                                </option>
                                                <option value="3" @if($user->proxy === 3) selected @endif>Muhasebe
                                                    Ofisi
                                                </option>
                                                <option value="4" @if($user->proxy === 4) selected @endif>Danışman
                                                </option>
                                                <option value="5" @if($user->proxy === 5) selected @endif>Tahsilat
                                                    Hizmetleri İşletmesi
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-md-4 control-label">Adınız</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name" disabled
                                                   value="{{ $user->name }}" required autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname" class="col-md-4 control-label">Soyadınız </label>

                                        <div class="col-md-6">
                                            <input id="surname" type="text" class="form-control" name="surname" disabled
                                                   value="{{ $user->surname }}" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="birth_year" class="col-md-4 control-label">Doğum
                                            Yılınız </label>

                                        <div class="col-md-6">
                                            <input id="birth_year" type="text" class="form-control"
                                                   name="birth_year" disabled
                                                   value="{{ $user->birth_year }}" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email" class="col-md-4 control-label">E-Mail Adresi</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" disabled
                                                   value="{{ $user->email }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tc_no" class="col-md-4 control-label">TC Kimlik No</label>

                                        <div class="col-md-6">
                                            <input id="tc_no" type="text" class="form-control" name="tc_no" disabled
                                                   value="{{ $user->tc_no }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('title_of_company') ? ' has-error' : '' }}">
                                        <label for="title_of_company" class="col-md-4 control-label">Şirket
                                            Ünvanı</label>

                                        <div class="col-md-6">
                                            <input id="title_of_company" type="text" class="form-control" disabled
                                                   name="title_of_company" value="{{ $user->title_of_company }}"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="company_tax_no" class="col-md-4 control-label">Vergi
                                            Numarası</label>

                                        <div class="col-md-6">
                                            <input id="company_tax_no" type="text" class="form-control" disabled
                                                   name="company_tax_no" value="{{ $user->company_tax_no }}"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tax_office_name" class="col-md-4 control-label">Vergi
                                            Dairesi</label>


                                        <div class="col-md-6">
                                            <input id="tax_office_name" type="text" class="form-control"
                                                   name="tax_office_name" disabled
                                                   value="{{ $user->tax_office_name }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="gsm" class="col-md-4 control-label">Cep Telefonu</label>

                                        <div class="col-md-6">
                                            <input id="gsm" type="text" class="form-control" name="gsm" disabled
                                                   value="{{ $user->gsm }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Şifre</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control"
                                                   name="password">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm" class="col-md-4 control-label">Şifre
                                            Tekrar</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="want_connection" class="col-md-4 control-label"></label>

                                        <div class="col-md-6">
                                            <input type="checkbox" name="want_connection" id="want_connection"
                                                   @if($user->want_connection == 0) checked @endif
                                                   value="0"><label for="want_connection"><span></span>
                                                <p>ALICININ DOĞRUDAN İLETİŞİME GEÇMESİNİ İSTEMİYORUM. İŞLEM
                                                    MUTABAKATINA KADAR İLETİŞİM VE BİLGİLERİMİN PAYLAŞILMASINI
                                                    İSTEMİYORUM. PLATFORM ÜZERİNDEN DOSYANIN TEYİDİNİ İSTİYORUM</p>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{__('text.update')}}
                                            </button>
                                            <a class="btn btn-warning" href="{{route('inside.index')}}">
                                                {{__('text.cancel')}}
                                            </a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection