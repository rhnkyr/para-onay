@extends('layouts.user-main')
@section('page-level-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('top-line')
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{__('labels.inside.new_case')}}</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection
@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-wallet font-green"></i>
                            <span class="caption-subject font-green bold uppercase">{{__('labels.inside.proxy_case_title')}}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['route' => 'inside.store', 'id'=>'addCase']) !!}

                        <h3>1- Temsil Edilen Şirket Bilgileri - Alacaklı Olunan (Hepsi macburi) Tablosu ayarlanacak!</h3>
                        <hr>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">{{__('labels.inside.title')}}</label>
                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'autofocus']); !!}
                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('tax_no') ? ' has-error' : '' }}">
                            <label for="tax_no" class="control-label">{{__('labels.inside.tax_no')}}</label>


                            {!! Form::text('tax_no', old('tax_no'), ['class' => 'form-control']); !!}
                            @if ($errors->has('tax_no'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_no') }}</strong>
                                    </span>
                            @endif

                        </div>


                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">{{__('labels.inside.responsible_name')}}</label>


                            {!! Form::text('name', old('name'), ['class' => 'form-control']); !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">{{__('labels.inside.responsible_email')}}</label>


                            {!! Form::text('email', old('email'), ['class' => 'form-control']); !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="control-label">{{__('labels.inside.responsible_phone')}}</label>


                            {!! Form::text('phone', old('phone'), ['class' => 'form-control']); !!}
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="control-label">{{__('labels.inside.city')}}</label>
                            {!! Form::select('city',$cities,old('city'),['class' => 'form-control select2']);!!}
                            @if ($errors->has('city'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                            @endif

                        </div>


                        <h3>2- Alacaklı Şirket Bilgileri (Sistemdeki adamın bilgileri gelecek)</h3>
                        <hr>
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">{{__('labels.inside.title')}}</label>
                            {!! Form::text('title', $user->title_of_company, ['class' => 'form-control', 'disabled']); !!}
                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('tax_no') ? ' has-error' : '' }}">
                            <label for="tax_no" class="control-label">{{__('labels.inside.tax_no')}}</label>


                            {!! Form::text('tax_no',  $user->company_tax_no, ['class' => 'form-control', 'disabled']); !!}
                            @if ($errors->has('tax_no'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_no') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('tax_office_name') ? ' has-error' : '' }}">
                            <label for="tax_office_name"
                                   class="control-label">{{__('labels.inside.tax_office_name')}}</label>


                            {!! Form::select('tax_office_name',
                            $tax_offices,
                            $user->tax_office_name,
                            ['class' => 'form-control select2 disabled']);
                            !!}
                            @if ($errors->has('tax_office_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_office_name') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('proxy_dept_amount') ? ' has-error' : '' }}">
                            <label for="dept_amount"
                                   class="control-label">{{__('labels.inside.proxy_dept_amount')}}</label>


                            {!! Form::text('proxy_dept_amount', old('proxy_dept_amount'), ['class' => 'form-control money']); !!}
                            @if ($errors->has('proxy_dept_amount'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('proxy_dept_amount') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('dept_currency') ? ' has-error' : '' }}">
                            <label for="dept_currency"
                                   class="control-label">{{__('labels.inside.dept_currency')}}</label>
                            {!! Form::select('dept_currency',
                            ['TL' => 'Türk Lirası', '$' => 'Dolar', '€' => 'Euro'],
                            'TL',
                            ['class' => 'form-control select2']);
                            !!}
                            @if ($errors->has('dept_currency'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('dept_currency') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('proxy_payable_dept') ? ' has-error' : '' }}">
                            <label for="proxy_payable_dept"
                                   class="control-label">{{__('labels.inside.proxy_payable_dept')}}</label>


                            {!! Form::text('proxy_payable_dept', old('proxy_payable_dept'), ['class' => 'form-control money']); !!}
                            @if ($errors->has('proxy_payable_dept'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('proxy_payable_dept') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('due_at') ? ' has-error' : '' }}">
                            <label for="due_at" class="control-label">{{__('labels.inside.due_at')}}</label>
                            <div class="input-group input-medium date date-picker" data-date=""
                                 data-date-format="mm-yyyy" data-date-viewmode="years"
                                 data-date-minviewmode="months">
                                {!! Form::text('due_at', old('due_at'), ['class' => 'form-control']); !!}
                                <span class="input-group-btn">
                                        <button class="btn default" type="button"><i
                                                    class="fa fa-calendar"></i></button>
                                    </span>
                            </div>
                            @if ($errors->has('due_at'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('due_at') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <hr>
                        <h3>3- Temlik Şirket Bilgileri - Borçlu Olunan (Hepsi macburi)</h3>
                        <hr>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">{{__('labels.inside.title')}}</label>
                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'autofocus']); !!}
                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('tax_no') ? ' has-error' : '' }}">
                            <label for="tax_no" class="control-label">{{__('labels.inside.tax_no')}}</label>


                            {!! Form::text('tax_no', old('tax_no'), ['class' => 'form-control']); !!}
                            @if ($errors->has('tax_no'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_no') }}</strong>
                                    </span>
                            @endif

                        </div>


                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">{{__('labels.inside.responsible_name')}}</label>


                            {!! Form::text('name', old('name'), ['class' => 'form-control']); !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">{{__('labels.inside.responsible_email')}}</label>


                            {!! Form::text('email', old('email'), ['class' => 'form-control']); !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="control-label">{{__('labels.inside.responsible_phone')}}</label>


                            {!! Form::text('phone', old('phone'), ['class' => 'form-control']); !!}
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="control-label">{{__('labels.inside.city')}}</label>
                            {!! Form::select('city',$cities,old('city'),['class' => 'form-control select2']);!!}
                            @if ($errors->has('city'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="row">
                            <div class="margin-top-10 pull-right">
                                <button type="submit" class="btn btn-success">
                                    {{__('text.save')}}
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-level-js')
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/apps/scripts/simple.money.format.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.tr.min.js')}}"
            type="text/javascript"></script>
    <script>
        $(function () {
            $('input[name="tax_no"]').mask('9999999999', {placeholder: ' '});
            $('input[name="phone"]').mask('0(999) 999-9999');
            $('.money').numeric().simpleMoneyFormat();
            $('.date-picker').datepicker({
                language: 'tr',
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
        });
    </script>
@endsection