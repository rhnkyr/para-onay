@extends('layouts.user-main')
@section('page-level-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('top-line')
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{__('labels.inside.new_case')}}</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection
@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- BEGIN PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-wallet font-green"></i>
                            <span class="caption-subject font-green bold uppercase">{{__('labels.inside.case_title')}}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        {!! Form::open(['route' => 'inside.store', 'id'=>'addCase']) !!}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">{{__('labels.inside.title')}}</label>
                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'autofocus']); !!}
                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('tax_no') ? ' has-error' : '' }}">
                            <label for="tax_no" class="control-label">{{__('labels.inside.tax_no')}}</label>


                            {!! Form::text('tax_no', old('tax_no'), ['class' => 'form-control']); !!}
                            @if ($errors->has('tax_no'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_no') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('tax_office_name') ? ' has-error' : '' }}">
                            <label for="tax_office_name"
                                   class="control-label">{{__('labels.inside.tax_office_name')}}</label>


                            {!! Form::select('tax_office_name',
                            $tax_offices,
                            null,
                            ['class' => 'form-control select2']);
                            !!}
                            @if ($errors->has('tax_office_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_office_name') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('dept_amount') ? ' has-error' : '' }}">
                            <label for="dept_amount"
                                   class="control-label">{{__('labels.inside.dept_amount')}}</label>


                            {!! Form::text('dept_amount', old('dept_amount'), ['class' => 'form-control money']); !!}
                            @if ($errors->has('dept_amount'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('dept_amount') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('dept_currency') ? ' has-error' : '' }}">
                            <label for="dept_currency"
                                   class="control-label">{{__('labels.inside.dept_currency')}}</label>
                            {!! Form::select('dept_currency',
                            ['TL' => 'Türk Lirası', '$' => 'Dolar', '€' => 'Euro'],
                            'TL',
                            ['class' => 'form-control select2']);
                            !!}
                            @if ($errors->has('dept_currency'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('dept_currency') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('payable_dept') ? ' has-error' : '' }}">
                            <label for="payable_dept"
                                   class="control-label">{{__('labels.inside.payable_dept')}}</label>


                            {!! Form::text('payable_dept', old('payable_dept'), ['class' => 'form-control money']); !!}
                            @if ($errors->has('payable_dept'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('payable_dept') }}</strong>
                                    </span>
                            @endif

                        </div>

                        <div class="form-group{{ $errors->has('due_at') ? ' has-error' : '' }}">
                            <label for="due_at" class="control-label">{{__('labels.inside.due_at')}}</label>
                            <div class="input-group input-medium date date-picker" data-date=""
                                 data-date-format="mm-yyyy" data-date-viewmode="years"
                                 data-date-minviewmode="months">
                                {!! Form::text('due_at', old('due_at'), ['class' => 'form-control']); !!}
                                <span class="input-group-btn">
                                        <button class="btn default" type="button"><i
                                                    class="fa fa-calendar"></i></button>
                                    </span>
                            </div>
                            @if ($errors->has('due_at'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('due_at') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <hr>
                        <h3 class="text-center">{{__('labels.inside.possible_buyers_title')}}</h3>
                        <br/>
                        <div class="mt-repeater">
                            <div data-repeater-list="pb">
                                @if(old('pb') !== null)
                                    <?php $index = 0; ?>
                                    @foreach(old('pb') as $pb)
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="{{ $errors->has('pb.'.$index.'.title') ? ' has-error' : '' }} row mt-repeater-row">
                                                <div class="col-md-10">
                                                    <label class="control-label">{{__('labels.inside.title')}}</label>
                                                    <input type="text" name="pb[{{$index}}][title]" class="form-control"
                                                           value="{{old('pb.'.$index.'.title')}}"/>
                                                    @if ($errors->has('pb.'.$index.'.title'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('pb.'.$index.'.title') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete
                                                       class="btn btn-danger mt-repeater-delete">
                                                        <i class="fa fa-close"></i> {{__('labels.inside.delete')}}
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row mt-repeater-row">
                                                <div class="{{ $errors->has('pb.'.$index.'.tax_no') ? ' has-error' : '' }}  col-md-6">
                                                    <label class="control-label">{{__('labels.inside.tax_no')}}</label>
                                                    <input type="text" name="pb[{{$index}}][tax_no]" class="form-control tax_no"
                                                           value="{{old('pb.'.$index.'.tax_no')}}"/>
                                                    @if ($errors->has('pb.'.$index.'.tax_no'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('pb.'.$index.'.tax_no') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">{{__('labels.inside.responsible_email')}}</label>
                                                    <input type="text" name="pb[{{$index}}][email]" class="form-control"
                                                           value="{{old('pb.'.$index.'.email')}}"/>
                                                </div>
                                            </div>
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-12">
                                                    <label class="control-label">{{__('labels.inside.responsible')}}</label>
                                                    <input type="text" name="pb[{{$index}}][responsible]" class="form-control"
                                                           value="{{old('pb.'.$index.'.responsible')}}"/>
                                                </div>
                                            </div>
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-6">
                                                    <label class="control-label">{{__('labels.inside.city')}}</label>
                                                    {!! Form::select('pb['.$index.'][city]',
                                                        $cities,
                                                        old('pb.'.$index.'.city'),
                                                        ['class' => 'form-control select2-container']);
                                                    !!}
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">{{__('labels.inside.phone')}}</label>
                                                    <input type="text" name="pb[{{$index}}][phone]" class="form-control phone"
                                                           value="{{old('pb.'.$index.'.phone')}}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $index++ ?>
                                    @endforeach
                                @else
                                    <div data-repeater-item class="mt-repeater-item">
                                        <div class="row mt-repeater-row">
                                            <div class="col-md-10">
                                                <label class="control-label">{{__('labels.inside.title')}}</label>
                                                <input type="text" name="title" class="form-control"/></div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete
                                                   class="btn btn-danger mt-repeater-delete">
                                                    <i class="fa fa-close"></i> {{__('labels.inside.delete')}}
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row mt-repeater-row">
                                            <div class="col-md-6">
                                                <label class="control-label">{{__('labels.inside.tax_no')}}</label>
                                                <input type="text" name="tax_no" class="form-control tax_no"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">{{__('labels.inside.responsible_email')}}</label>
                                                <input type="text" name="email" class="form-control"/></div>
                                        </div>
                                        <div class="row mt-repeater-row">
                                            <div class="col-md-12">
                                                <label class="control-label">{{__('labels.inside.responsible')}}</label>
                                                <input type="text" name="responsible" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="row mt-repeater-row">
                                            <div class="col-md-6">
                                                <label class="control-label">{{__('labels.inside.city')}}</label>
                                                {!! Form::select('city',
                                                    $cities,
                                                    null,
                                                    ['class' => 'form-control select2-container']);
                                                !!}
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">{{__('labels.inside.phone')}}</label>
                                                <input type="text" name="phone" class="form-control phone"/>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row text-center">
                                <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                    <i class="fa fa-plus"></i> {{__('labels.inside.add_possible_buyer')}}</a>
                            </div>
                        </div>


                        <div class="row">
                            <div class="margin-top-10 pull-right">
                                <button type="submit" class="btn btn-success">
                                    {{__('text.save')}}
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-level-js')
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/apps/scripts/simple.money.format.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.tr.min.js')}}"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"
            type="text/javascript"></script>
    <script>
        $(function () {
            $('input[name="tax_no"]').mask('9999999999', {placeholder: ' '});
            $('input[name="phone"]').mask('0(999) 999-9999');
            $('.money').numeric().simpleMoneyFormat();
            $('.date-picker').datepicker({
                language: 'tr',
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });

            $('.mt-repeater').repeater({
                @if(!$errors->has('*'))
                initEmpty: true,
                @endif
                show: function () {
                    $(this).slideDown();
                    $('.tax_no').mask('9999999999', {placeholder: ' '});
                    $('.phone').mask('0(999) 999-9999');

                    $(this).find('.select2-container').select2({
                        placeholder: "Şehir Seçiniz"
                    });

                    $('.select2-container').css('width', '100%');

                },
                hide: function (deleteElement) {
                    bootbox.confirm({
                        size: 'small',
                        title: 'Bilgi',
                        message: "<center>{{__('labels.inside.confirm_delete')}}</center>",
                        buttons: {
                            'cancel': {
                                label: 'Vazgeç',
                                className: 'btn-default'
                            },
                            'confirm': {
                                label: 'Sil',
                                className: 'btn-danger pull-right'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $(this).slideUp(deleteElement);
                            }
                        }
                    });
                },
                ready: function (setIndexes) {

                }
            });
        });
    </script>
@endsection