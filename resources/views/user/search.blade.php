@extends('layouts.user-main')
@section('page-level-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('top-line')
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{__('labels.inside.search_company')}}</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection
@section('content')
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            {!! Form::open(['route' => 'search-post', 'id'=>'search']) !!}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('tax_number') ? ' has-error' : '' }}">
                                            <label class="control-label">Vergi Numarası</label>
                                            {!! Form::text('tax_number', old('tax_number'), ['class' => 'form-control', 'autofocus']); !!}
                                            @if ($errors->has('tax_number'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tax_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label class="control-label">Ünvan</label>
                                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'autofocus']); !!}
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions text-right">
                                <button type="submit" class="btn btn-success"><i
                                            class="fa fa-check"></i> {{__('labels.inside.search')}}</button>
                            </div>
                            {!! Form::close() !!}

                            @if(isset($cases))
                                @if($cases->count() > 0)
                                    <div class="row">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th> Olası Şirket</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cases as $case)
                                                <tr>
                                                    <td>{{$case->title}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @else
                                    <div class="alert alert-info text-center">
                                        <h3><strong>{{__('labels.inside.info')}}
                                                !</strong> {{__('labels.inside.inform_me')}}</h3>
                                        <br><a href="#"
                                               class="info btn btn-success margin-top-15">{{__('labels.inside.inform_me_button')}}</a>
                                    </div>
                                    <div class="row text-center">
                                        <h2> YADA </h2>
                                        <br>
                                    </div>
                                    <div class="alert alert-info text-center">
                                        <h3><strong>{{__('labels.inside.info')}}
                                                !</strong> {{__('labels.inside.add_possible_seller')}}</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-1">
                                            {!! Form::open(['route' => 'save-possible-seller', 'class'=>'form-horizontal','id'=>'addPossibleSeller']) !!}
                                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                                <label for="title"
                                                       class="col-md-4 control-label">{{__('labels.inside.title')}}</label>

                                                <div class="col-md-8">
                                                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'autofocus']); !!}
                                                    @if ($errors->has('title'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('tax_no') ? ' has-error' : '' }}">
                                                <label for="tax_no"
                                                       class="col-md-4 control-label">{{__('labels.inside.tax_no')}}</label>

                                                <div class="col-md-4">
                                                    {!! Form::text('tax_no', old('tax_no'), ['class' => 'form-control','placeholder'=>__('labels.inside.tax_no_placeholder')]); !!}
                                                    @if ($errors->has('tax_no'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('tax_no') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="city"
                                                       class="col-md-4 control-label">{{__('labels.inside.responsible')}}</label>

                                                <div class="col-md-4">
                                                    {!! Form::text('responsible', old('responsible'), ['class' => 'form-control']); !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="city"
                                                       class="col-md-4 control-label">{{__('labels.inside.responsible_email')}}</label>

                                                <div class="col-md-4">
                                                    {!! Form::text('email', old('email'), ['class' => 'form-control']); !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="city"
                                                       class="col-md-4 control-label">{{__('labels.inside.city')}}</label>

                                                <div class="col-md-4">
                                                    {!! Form::select('city',
                                                    $cities,
                                                    old('city'),
                                                    ['class' => 'form-control city-select']);
                                                    !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="phone"
                                                       class="col-md-4 control-label">{{__('labels.inside.phone')}}</label>

                                                <div class="col-md-4">
                                                    {!! Form::text('phone', old('phone'), ['class' => 'form-control']); !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{__('text.save')}}
                                                    </button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-level-js')
    @if(isset($cases))
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script>
            $(function () {
                $('input[name="tax_no"]').mask('9999999999', {placeholder: ' '});
                $('input[name="phone"]').mask('0(999) 999-9999');
                $('.city-select').select2({
                    placeholder: "Şehir Seçiniz"
                });
                $('.info').on('click', function (e) {
                    e.preventDefault();
                    $.post('{{route('save-inform')}}', {keyword: '{{$param}}'}, function (res) {
                        if (res) {
                            location.href = '{{route('search-get')}}';
                        } else {
                            toastr.error(res.message, '{{__('labels.inside.info')}}');
                        }
                    }, 'json');
                });
            });
        </script>
    @else
        <script>
            $(function () {
                $('input[name="tax_number"]').mask('9999999999', {placeholder: ' '});
            });
        </script>
    @endif
@endsection
