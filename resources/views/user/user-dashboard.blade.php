@extends('layouts.user-main')
@section('top-line')
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{__('labels.inside.dashboard')}}</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection
@section('content')
    <div class="row widget-row">
        <div class="col-md-4">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">{{__('labels.given_cases')}}</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-square-o"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup"
                              data-value="{{count($active_cases)}}">{{count($active_cases)}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-4">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">{{__('labels.suspended_cases')}}</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red fa fa-pause"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup"
                              data-value="{{count($suspended_cases)}}">{{count($suspended_cases)}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-4">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                <h4 class="widget-thumb-heading">{{__('labels.closed_cases')}}</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple fa fa-check-square-o"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup"
                              data-value="{{count($closed_cases)}}">{{count($closed_cases)}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row widget-row">
        <div class="portlet box white">
            <div class="portlet-title">
                <div class="caption col-md-12">
                    <i class="fa fa-file-o"></i>Dosyalarım
                    <a class="btn blue pull-right" href="{{route('inside.create')}}"> Yeni
                        Ekle </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_15_1" data-toggle="tab"
                               aria-expanded="true"> {{__('labels.given_cases')}} </a>
                        </li>
                        <li class="">
                            <a href="#tab_15_2" data-toggle="tab"
                               aria-expanded="false"> {{__('labels.suspended_cases')}} </a>
                        </li>
                        <li class="">
                            <a href="#tab_15_3" data-toggle="tab"
                               aria-expanded="false"> {{__('labels.closed_cases')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_15_1">
                            <div class="table-scrollable">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th> Dosya No</th>
                                        <th> Alacaklı</th>
                                        <th> Tutar</th>
                                        <th> Talep Edilen Tutar</th>
                                        <th> Birim</th>
                                        <th> Talep İndirim Oranı(%)</th>
                                        <th> Vade</th>
                                        <th> Açılış Tarihi</th>
                                        <th> Sözleşme</th>
                                        <th> İşlem</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($active_cases as $case)
                                        <tr>
                                            <td>{{$case->case_no}}</td>
                                            <td>{{$case->title}}</td>
                                            <td>{{$case->dept_amount}}</td>
                                            <td>{{$case->payable_dept}}</td>
                                            <td>{{$case->dept_currency}}</td>
                                            <td>{{$case->discount}}</td>
                                            <td>{{$case->due_at}}</td>
                                            <td>{{date('d-m-Y',strtotime($case->created_at))}}</td>
                                            <td><a href="{{route('show-contract', ['path'=> $case->case_no])}}"
                                                   target="_blank"
                                                   class="label label-sm label-info">{{__('labels.inside.lookup')}}</a>
                                            </td>
                                            <td>
                                                <a href="{{route('inside.edit', ['inside'=> $case->id])}}"
                                                   class="label label-sm label-success">{{__('labels.inside.edit')}}</a>
                                                <a href="{{route('inside.edit', ['inside'=> $case->id])}}"
                                                   data-id="{{$case->id}}"
                                                   class="label label-sm label-warning suspendCase">{{__('labels.inside.suspend')}}</a>
                                                <a href="#" class="label label-sm label-danger delCase"
                                                   data-id="{{$case->id}}">{{__('labels.inside.delete')}}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_15_2">
                            <div class="table-scrollable">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th> Dosya No</th>
                                        <th> Alacaklı</th>
                                        <th> Tutar</th>
                                        <th> Talep Edilen Tutar</th>
                                        <th> Birim</th>
                                        <th> Talep İndirim Oranı(%)</th>
                                        <th> Vade</th>
                                        <th> Açılış Tarihi</th>
                                        <th> Sözleşme</th>
                                        <th> İşlem</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($suspended_cases as $case)
                                        <tr>
                                            <td>{{$case->case_no}}</td>
                                            <td>{{$case->title}}</td>
                                            <td>{{$case->dept_amount}}</td>
                                            <td>{{$case->payable_dept}}</td>
                                            <td>{{$case->dept_currency}}</td>
                                            <td>{{$case->discount}}</td>
                                            <td>{{$case->due_at}}</td>
                                            <td>{{date('d-m-Y',strtotime($case->created_at))}}</td>
                                            <td><a href="{{route('show-contract', ['path'=> $case->case_no])}}"
                                                   target="_blank"
                                                   class="label label-sm label-info">{{__('labels.inside.lookup')}}</a>
                                            </td>
                                            <td>
                                                <a href="{{route('inside.edit', ['inside'=> $case->id])}}"
                                                   class="label label-sm label-success">{{__('labels.inside.edit')}}</a>
                                                <a href="{{route('inside.edit', ['inside'=> $case->id])}}"
                                                   data-id="{{$case->id}}"
                                                   class="label label-sm label-info suspendCase">{{__('labels.inside.active')}}</a>
                                                <a href="#" class="label label-sm label-danger delCase"
                                                   data-id="{{$case->id}}">{{__('labels.inside.delete')}}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_15_3">
                            <div class="table-scrollable">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th> Dosya No</th>
                                        <th> Alacaklı</th>
                                        <th> Tutar</th>
                                        <th> Talep Edilen Tutar</th>
                                        <th> Birim</th>
                                        <th> Talep İndirim Oranı(%)</th>
                                        <th> Vade</th>
                                        <th> Açılış Tarihi</th>
                                        <th> Kapanış Tarihi</th>
                                        <th> Sözleşme</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($closed_cases as $case)
                                        <tr>
                                            <td>{{$case->casa_no}}</td>
                                            <td>{{$case->title}}</td>
                                            <td>{{$case->dept_amount}}</td>
                                            <td>{{$case->payable_dept}}</td>
                                            <td>{{$case->dept_currency}}</td>
                                            <td>{{$case->discount}}</td>
                                            <td>{{$case->due_at}}</td>
                                            <td>{{date('d-m-Y',strtotime($case->created_at))}}</td>
                                            <td>{{date('d-m-Y',strtotime($case->closed_at))}}</td>
                                            <td><a href="{{route('show-contract', ['path'=> $case->case_no])}}"
                                                   target="_blank"
                                                   class="label label-sm label-info">{{__('labels.inside.lookup')}}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-level-js')
    <script>
        $(function () {
            $('.delCase').click(function (e) {
                var self = $(this);
                e.preventDefault();
                bootbox.confirm({
                    size: 'small',
                    title: 'Bilgi',
                    message: "<center>{{__('labels.inside.confirm_delete')}}</center>",
                    buttons: {
                        'cancel': {
                            label: 'Vazgeç',
                            className: 'btn-default'
                        },
                        'confirm': {
                            label: 'Sil',
                            className: 'btn-danger pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                type: 'DELETE',
                                url: "{!! url('company/inside' ) !!}" + "/" + self.data('id'),
                                dataType: 'json',
                                success: function (res) {
                                    if (res.status) {
                                        location.reload();
                                    }
                                },
                                error: function (res) {
                                    toastr.error(res.message, '{{__('labels.inside.info')}}');
                                }
                            });
                        }
                    }
                });
            });

            $('.suspendCase').click(function (e) {
                var self = $(this);
                e.preventDefault();
                bootbox.confirm({
                    size: 'small',
                    title: 'Bilgi',
                    message: "<center>{{__('labels.inside.confirm_status')}}</center>",
                    buttons: {
                        'cancel': {
                            label: 'Vazgeç',
                            className: 'btn-default'
                        },
                        'confirm': {
                            label: 'Evet',
                            className: 'btn-info pull-right'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                type: 'POST',
                                url: "{{route('case-status')}}",
                                dataType: 'json',
                                data : {id : self.data('id')},
                                success: function (res) {
                                    if (res.status) {
                                        location.reload();
                                    }
                                },
                                error: function (res) {
                                    toastr.error(res.message, '{{__('labels.inside.info')}}');
                                }
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection