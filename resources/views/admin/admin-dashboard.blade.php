@extends('layouts.admin-main')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        Güncel Bilgiler
                    </h3>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin:: Widgets/Stats-->
            <div class="m-portlet ">
                <div class="m-portlet__body  m-portlet__body--no-padding">
                    <div class="row m-row--no-padding m-row--col-separator-xl">
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        Total Frofit
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
														All Customs Value
													</span>
                                    <span class="m-widget24__stats m--font-brand">
														$18M
													</span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;"
                                             aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
														Change
													</span>
                                    <span class="m-widget24__number">
														78%
													</span>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::New Feedbacks-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        New Feedbacks
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
														Customer Review
													</span>
                                    <span class="m-widget24__stats m--font-info">
														1349
													</span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;"
                                             aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
														Change
													</span>
                                    <span class="m-widget24__number">
														84%
													</span>
                                </div>
                            </div>
                            <!--end::New Feedbacks-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::New Orders-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        New Orders
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
														Fresh Order Amount
													</span>
                                    <span class="m-widget24__stats m--font-danger">
														567
													</span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;"
                                             aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
														Change
													</span>
                                    <span class="m-widget24__number">
														69%
													</span>
                                </div>
                            </div>
                            <!--end::New Orders-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::New Users-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        New Users
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
														Joined New User
													</span>
                                    <span class="m-widget24__stats m--font-success">
														276
													</span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;"
                                             aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
														Change
													</span>
                                    <span class="m-widget24__number">
														90%
													</span>
                                </div>
                            </div>
                            <!--end::New Users-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Stats-->

            <!--Begin::Section-->
            <div class="row">
                <div class="col-xl-6">
                    <!--begin:: Widgets/Support Cases-->
                    <div class="m-portlet  m-portlet--full-height ">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Support Cases
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                        data-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#"
                                           class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl m-dropdown__toggle">
                                            <i class="la la-ellipsis-h m--font-brand"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__section m-nav__section--first">
																				<span class="m-nav__section-text">
																					Quick Actions
																				</span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-share"></i>
                                                                    <span class="m-nav__link-text">
																						Activity
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                    <span class="m-nav__link-text">
																						Messages
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-info"></i>
                                                                    <span class="m-nav__link-text">
																						FAQ
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                    <span class="m-nav__link-text">
																						Support
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__separator m-nav__separator--fit"></li>
                                                            <li class="m-nav__item">
                                                                <a href="#"
                                                                   class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
                                                                    Cancel
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget16">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="m-widget16__head">
                                            <div class="m-widget16__item">
																<span class="m-widget16__sceduled">
																	Type
																</span>
                                                <span class="m-widget16__amount m--align-right">
																	Amount
																</span>
                                            </div>
                                        </div>
                                        <div class="m-widget16__body">
                                            <!--begin::widget item-->
                                            <div class="m-widget16__item">
																<span class="m-widget16__date">
																	EPS
																</span>
                                                <span class="m-widget16__price m--align-right m--font-brand">
																	+78,05%
																</span>
                                            </div>
                                            <!--end::widget item-->
                                            <!--begin::widget item-->
                                            <div class="m-widget16__item">
																<span class="m-widget16__date">
																	PDO
																</span>
                                                <span class="m-widget16__price m--align-right m--font-accent">
																	21,700
																</span>
                                            </div>
                                            <!--end::widget item-->
                                            <!--begin::widget item-->
                                            <div class="m-widget16__item">
																<span class="m-widget16__date">
																	OPL Status
																</span>
                                                <span class="m-widget16__price m--align-right m--font-danger">
																	Negative
																</span>
                                            </div>
                                            <!--end::widget item-->
                                            <!--begin::widget item-->
                                            <div class="m-widget16__item">
																<span class="m-widget16__date">
																	Priority
																</span>
                                                <span class="m-widget16__price m--align-right m--font-brand">
																	+500,200
																</span>
                                            </div>
                                            <!--end::widget item-->
                                            <!--begin::widget item-->
                                            <div class="m-widget16__item">
																<span class="m-widget16__date">
																	Net Prifit
																</span>
                                                <span class="m-widget16__price m--align-right m--font-brand">
																	$18,540,60
																</span>
                                            </div>
                                            <!--end::widget item-->
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="m-widget16__stats">
                                            <div class="m-widget16__visual">
                                                <div id="m_chart_support_tickets" style="height: 180px"></div>
                                            </div>
                                            <div class="m-widget16__legends">
                                                <div class="m-widget16__legend">
                                                    <span class="m-widget16__legend-bullet m--bg-info"></span>
                                                    <span class="m-widget16__legend-text">
																		20% Margins
																	</span>
                                                </div>
                                                <div class="m-widget16__legend">
                                                    <span class="m-widget16__legend-bullet m--bg-accent"></span>
                                                    <span class="m-widget16__legend-text">
																		80% Profit
																	</span>
                                                </div>
                                                <div class="m-widget16__legend">
                                                    <span class="m-widget16__legend-bullet m--bg-danger"></span>
                                                    <span class="m-widget16__legend-text">
																		10% Lost
																	</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/Support Stats-->
                </div>
                <div class="col-xl-6">
                    <!--begin:: Widgets/Finance Stats-->
                    <div class="m-portlet  m-portlet--full-height ">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Finance Stats
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                        data-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#"
                                           class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl m-dropdown__toggle">
                                            <i class="la la-ellipsis-h m--font-brand"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav">
                                                            <li class="m-nav__section m-nav__section--first">
																				<span class="m-nav__section-text">
																					Quick Actions
																				</span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-share"></i>
                                                                    <span class="m-nav__link-text">
																						Activity
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                    <span class="m-nav__link-text">
																						Messages
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-info"></i>
                                                                    <span class="m-nav__link-text">
																						FAQ
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                    <span class="m-nav__link-text">
																						Support
																					</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__separator m-nav__separator--fit"></li>
                                                            <li class="m-nav__item">
                                                                <a href="#"
                                                                   class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
                                                                    Cancel
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget1 m-widget1--paddingless">
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">
                                                IPO Margin
                                            </h3>
                                            <span class="m-widget1__desc">
																Awerage IPO Margin
															</span>
                                        </div>
                                        <div class="col m--align-right">
															<span class="m-widget1__number m--font-accent">
																+24%
															</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">
                                                Payments
                                            </h3>
                                            <span class="m-widget1__desc">
																Yearly Expenses
															</span>
                                        </div>
                                        <div class="col m--align-right">
															<span class="m-widget1__number m--font-info">
																+$560,800
															</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">
                                                Logistics
                                            </h3>
                                            <span class="m-widget1__desc">
																Overall Regional Logistics
															</span>
                                        </div>
                                        <div class="col m--align-right">
															<span class="m-widget1__number m--font-warning">
																-10%
															</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">
                                                Expenses
                                            </h3>
                                            <span class="m-widget1__desc">
																Balance
															</span>
                                        </div>
                                        <div class="col m--align-right">
															<span class="m-widget1__number m--font-danger">
																$345,000
															</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/Finance Stats-->
                </div>
            </div>
            <!--End::Section-->

            <!--Begin::Section-->
        </div>
    </div>
@endsection
@section('page-vendors-js')
    <script src="{{asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
@endsection