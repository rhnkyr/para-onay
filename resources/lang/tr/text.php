<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email'              => 'Eposta',
    'password'           => 'Şifre',
    'login'              => 'Giriş',
    'save'               => 'Kaydet',
    'cancel'             => 'Vazgeç',
    'update'             => 'Güncelle',
    'remember_me'        => 'Beni Hatırla',
    'forget_password'    => 'Şifremi Unuttum',
    'info'               => 'Bilgi!',
    'error'              => 'Hata!',
    'sms_and_email_info' => 'Sms ve email adresi onaylarınızı yaptığınızdan emin olunuz. Aksi takdirde sisteme giriş yapamazsınız!',
    'sms_place_holder'   => 'Lütfen cep telefonunuza gelen sms şifresini yazınız',
    'sms_error'          => 'Girdiğiniz sms kodu yanlıştır. Lütfen tekrar deneyiniz!',
    'sms_approve'        => 'SMS Onay',
    'email_approve'      => 'Email Onay',
    'email_info'         => 'Mail adresinize gönderilen postayı onaylayınız!',
    'control_button'     => 'Gönder',

    'user' => [
        'profile' => 'Profilim',
        'logout'  => 'Çıkış'
    ]

];
