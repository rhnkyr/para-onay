<?php

namespace Tests\Feature;

use App\Enums\UserType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSellerOrBuyer()
    {

        $type = 3;

        if (\in_array($type, [UserType::SELLER, UserType::BUYER])) {
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }


    }
}
