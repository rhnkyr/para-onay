<?php

namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        \Validator::extend('lower_than_field', function ($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data      = $validator->getData();
            $min_value = (float)str_replace(',', '', $data[$min_field]);
            return (float)str_replace(',', '', $value) < $min_value;
        });

        \Validator::replacer('lower_than_field', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() === 'local') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        \Schema::defaultStringLength(191);
    }
}
