<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Jul 2018 08:52:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Reservation
 *
 * @property int $id
 * @property string $user_id
 * @property string $case_identifier
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Reservation extends Eloquent
{
    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'case_identifier',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cases()
    {
        return $this->hasMany(DeptCase::class, 'case_identifier', 'case_identifier');
    }

    public function scopeUserApprovedReservation($query)
    {
        return $query->where('user_id', \Auth::user()->id)->where('status', 1)->latest();
    }


}
