<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 09 Jul 2018 13:17:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Price
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property float $price
 * @property int $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Price extends Eloquent
{
	protected $casts = [
		'price' => 'float',
		'active' => 'int'
	];

	protected $fillable = [
		'name',
		'slug',
		'price',
		'active'
	];
}
