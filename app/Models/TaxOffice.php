<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Jul 2018 09:44:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TaxOffice
 * 
 * @property int $id
 * @property string $plate
 * @property string $city
 * @property string $district
 * @property string $office
 *
 * @package App\Models
 */
class TaxOffice extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'plate',
		'city',
		'district',
		'office'
	];
}
