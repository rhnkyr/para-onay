<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 09 Jul 2018 13:17:25 +0000.
 */

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;
use Spatie\Permission\Traits\HasRoles;


/**
 * Class User
 *
 * @property string $id
 * @property string $name
 * @property string $surname
 * @property int $birth_year
 * @property string $title_of_user
 * @property string $industry
 * @property string $num_connections
 * @property string $public_profile_url
 * @property string $tc_no
 * @property string $title_of_company
 * @property string $company_tax_no
 * @property string $tax_office_name
 * @property string $email
 * @property string $gsm
 * @property int $city_id
 * @property string $password
 * @property string $ip
 * @property int $sms
 * @property int $login_type
 * @property int $type
 * @property string $email_hash
 * @property int $sms_approved
 * @property int $email_approved
 * @property int $proxy
 * @property int $want_connection
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable, HasRoles, UuidForKey/*, Searchable*/;

    public $incrementing = false;

    protected $casts = [
        'birth_year'      => 'int',
        'sms'             => 'int',
        'login_type'      => 'int',
        'type'            => 'int',
        'sms_approved'    => 'int',
        'email_approved'  => 'int',
        'proxy'           => 'int',
        'want_connection' => 'int'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $fillable = [
        'id',
        'name',
        'surname',
        'birth_year',
        'title_of_user',
        'industry',
        'num_connections',
        'public_profile_url',
        'title_of_company',
        'company_tax_no',
        'tax_office_name',
        'tc_no',
        'email',
        'gsm',
        'city_id',
        'password',
        'ip',
        'sms',
        'login_type',
        'type',
        'email_hash',
        'sms_approved',
        'email_approved',
        'proxy',
        'want_connection',
        'remember_token'
    ];

    public function searchableAs()
    {
        return 'users_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['title_of_company', 'company_tax_no', 'tax_office_name']);
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->name} {$this->surname}";
    }

    public function invites()
    {
        return $this->hasMany(Invite::class);
    }

    public function search_logs()
    {
        return $this->hasMany(SearchLog::class);
    }

    /**
     * Find user by email
     * @param $email
     * @return User|\Illuminate\Database\Eloquent\Model|null|object
     */
    public static function findByEmail($email)
    {
        return (new static)->where('email', $email)->first();
    }
}
