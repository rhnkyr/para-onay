<?php

    /**
     * Created by Reliese Model.
     * Date: Wed, 01 Aug 2018 06:01:35 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class SearchLog
     *
     * @property int            $id
     * @property string         $user_id
     * @property string         $title
     * @property string         $tax_no
     * @property int            $has_found
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class SearchLog extends Eloquent
    {
        protected $casts
          = [
            'has_found' => 'int',
          ];

        protected $fillable
          = [
            'user_id',
            'tax_no',
            'title',
            'has_found',
          ];


        public function user()
        {
            return $this->hasOne(User::class);
        }

    }
