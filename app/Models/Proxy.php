<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 09 Jul 2018 13:17:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Proxy
 * 
 * @property int $id
 * @property string $name
 * @property int $is_active
 *
 * @package App\Models
 */
class Proxy extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'is_active' => 'int'
	];

	protected $fillable = [
		'name',
		'is_active'
	];
}
