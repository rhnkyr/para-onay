<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Jul 2018 08:59:13 +0000.
 */

namespace App\Models;

use App\Traits\UuidForKey;
use Laravel\Scout\Searchable;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DeptCase
 *
 * @property int $id
 * @property string $case_no
 * @property string $user_id
 * @property string $title
 * @property string $tax_no
 * @property string $tax_office_name
 * @property string $city_id
 * @property float $dept_amount
 * @property string $dept_currency
 * @property float $payable_dept
 * @property float $discount
 * @property \Carbon\Carbon $due_at
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $closed_at
 *
 * @package App\Models
 */
class DeptCase extends Eloquent
{

    use UuidForKey/*, Searchable*/; //todo : servera atınca açılacak

    public $incrementing = false;

    protected $casts = [
        'dept_amount'  => 'float',
        'payable_dept' => 'float',
        'discount'     => 'float',
        'status'       => 'int'
    ];

    protected $dates = [
        'due_at'
    ];

    protected $fillable = [
        'case_no',
        'user_id',
        'title',
        'tax_no',
        'tax_office_name',
        'city_id',
        'dept_amount',
        'dept_currency',
        'payable_dept',
        'discount',
        'due_at',
        'closed_at',
        'status'
    ];

    public function searchableAs()
    {
        return 'dept_cases_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['title', 'tax_no', 'tax_office_name']);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class, 'case_identifier', 'case_identifier');
    }


    public function getDeptAmountAttribute()
    {
        return number_format($this->attributes['dept_amount']);
    }

    public function getPayableDeptAttribute()
    {
        return number_format($this->attributes['payable_dept']);
    }

    public function getDueAtAttribute()
    {
        return $this->attributes['due_at'] === null ? '-' : date('m-Y', strtotime($this->attributes['due_at']));
    }


    public function scopeGetCases($query, $status)
    {
        return $query->where('user_id', \Auth::user()->id)->where('status', $status)->latest();
    }

    public function invites(){
        return $this->hasMany(Invite::class,'case_id','id');
    }

}
