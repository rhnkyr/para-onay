<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Jul 2018 13:17:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Invite
 *
 * @property int $id
 * @property string $user_id
 * @property string $case_id
 * @property string $title
 * @property string $email
 * @property string $tax_no
 * @property string $responsible
 * @property string $city
 * @property string $phone
 * @property int $type
 * @property int $is_main
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Invite extends Eloquent
{
    protected $casts = [
        'type' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'case_id',
        'title',
        'email',
        'tax_no',
        'responsible',
        'city',
        'phone',
        'type',
        'is_main'
    ];
}
