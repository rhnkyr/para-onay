<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 25 Jul 2018 06:14:47 +0000.
 */

namespace App\Models;

use Laravel\Scout\Searchable;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Reminder
 *
 * @property int $id
 * @property string $user_id
 * @property string $keyword
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Reminder extends Eloquent
{

    use Searchable;

    protected $fillable = [
        'user_id',
        'keyword'
    ];

    public function searchableAs()
    {
        return 'reminders_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['keyword']);
    }

}
