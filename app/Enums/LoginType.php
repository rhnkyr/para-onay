<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class LoginType extends Enum
{
    const EMAIL = 1;
    const LINKEDIN = 2;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value): string
    {
        if ($value === self::EMAIL) {
            return 'Email';
        }

        if ($value === self::LINKEDIN) {
            return 'Linkedin';
        }

        return parent::getDescription($value);
    }
}
