<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class CaseStatus extends Enum
{
    const Open = 1;
    const Closed = 2;
    const Suspended = 3;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value): string
    {
        if ($value === self::Open) {
            return 'Açık';
        }

        if ($value === self::Closed) {
            return 'Kapalı';
        }

        if ($value === self::Suspended) {
            return 'Askıda';
        }

        return parent::getDescription($value);
    }
}
