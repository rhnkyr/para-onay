<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ReservationType extends Enum
{
    const UnApproved = 0;
    const Approved = 1;

    /**
     * Get the description for an enum value
     *
     * @param $value
     * @return string
     */
    public static function getDescription($value): string
    {
        if ($value === self::UnApproved) {
            return 'Teyitsiz Rezervasyon';
        }

        if ($value === self::Approved) {
            return 'Teyitli Rezervasyon';
        }

        return parent::getDescription($value);
    }
}
