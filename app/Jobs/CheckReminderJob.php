<?php

namespace App\Jobs;

use App\Mail\ReminderEmail;
use App\Models\Reminder;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckReminderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reminders = Reminder::where('keyword', $this->user->tax_no)->get();

        if ($reminders) {
            foreach ($reminders as $reminder) {
                $user = User::findOrFail($reminder->user_id);
                //todo : burasının mail işlemleri ayarlanacak
                //\Mail::to($user->email)->send(new ReminderEmail($user->name_surname));
            }
        }
    }
}
