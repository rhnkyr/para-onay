<?php

namespace App\Jobs;

use App\Mail\RegisterEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RegisterEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $name_surname;
    private $email;
    private $mail_hash;

    /**
     * Create a new job instance.
     *
     * @param $name_surname
     * @param $email
     */
    public function __construct($name_surname, $email, $mail_hash)
    {
        $this->email        = $email;
        $this->name_surname = $name_surname;
        $this->mail_hash    = $mail_hash;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->email)->send(new RegisterEmail($this->name_surname, $this->mail_hash));
    }
}
