<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $phone;
    private $key;

    /**
     * Create a new job instance.
     *
     * @param $phone
     * @param $key
     */
    public function __construct($phone, $key)
    {
        $this->phone = $phone;
        $this->key   = $key;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //todo send sms
    }
}
