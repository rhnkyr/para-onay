<?php

namespace App\Http\Middleware;

use Closure;

class CheckBuyer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = \Auth::user();
        if (!$user->hasRole('buyer')) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
