<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CaseEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dept_amount'   => 'required',
            'dept_currency' => 'required',
            'payable_dept'  => 'required|lower_than_field:dept_amount',
            'pb.*.title'    => 'required',
            'pb.*.tax_no'   => 'digits:10',
        ];
    }
}
