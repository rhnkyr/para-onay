<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PossibleSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|string|max:190',
            'tax_no'      => 'digits:10',
            'responsible' => 'nullable|string|max:190',
            'city'        => 'nullable|string|max:190',
            'email'       => 'nullable|email|max:190',
            'phone'       => 'nullable|string|max:190'
        ];
    }
}
