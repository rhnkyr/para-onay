<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function chooser()
    {
        return view('site.chooser');
    }

    public function faq()
    {
        return view('site.faq');
    }
}
