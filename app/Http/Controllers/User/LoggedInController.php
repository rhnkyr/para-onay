<?php

    namespace App\Http\Controllers\User;

    use App\Enums\CaseStatus;
    use App\Enums\InviteType;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\CaseEditRequest;
    use App\Http\Requests\CaseRequest;
    use App\Http\Requests\PossibleSellerRequest;
    use App\Http\Requests\SearchRequest;
    use App\Http\Requests\SellerRequest;
    use App\Models\City;
    use App\Models\DeptCase;
    use App\Models\Invite;
    use App\Models\Reminder;
    use App\Models\SearchLog;
    use App\Models\TaxOffice;
    use App\Models\User;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class LoggedInController extends Controller
    {
        /*public function __construct()
        {
            $this->middleware('auth');

        }*/

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $active_cases    = DeptCase::getCases(CaseStatus::Open)->get();
            $closed_cases    = DeptCase::getCases(CaseStatus::Closed)->get();
            $suspended_cases = DeptCase::getCases(CaseStatus::Suspended)->get();

            //$reserved_cases = Reservation::userApprovedReservation()->get();

            return view('user.user-dashboard', compact('active_cases', 'closed_cases', 'suspended_cases'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $tax_offices = TaxOffice::pluck('office', 'id');
            $cities      = City::pluck('name', 'name');

            return view('user.add-case', compact('tax_offices', 'cities'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param CaseRequest $request
         *
         * @return \Illuminate\Http\RedirectResponse
         * @throws \Exception
         */
        public function store(CaseRequest $request)
        {
            $user          = \Auth::user();
            $dept_amount   = str_replace(',', '', $request->dept_amount);
            $payable_dept  = str_replace(',', '', $request->payable_dept);
            $percentChange = $this->percentage($dept_amount, $payable_dept);
            $office        = TaxOffice::findOrFail($request->tax_office_name);
            $case_no       = $this->caseName();
            $path          = $user->id . DIRECTORY_SEPARATOR . $case_no . '.pdf';

            $case                  = new DeptCase();
            $case->case_no         = $case_no;
            $case->user_id         = \Auth::user()->id;
            $case->title           = $request->title;
            $case->tax_no          = $request->tax_no;
            $case->tax_office_name = $office->office;
            $case->city_id         = $office->plate;
            $case->dept_amount     = $dept_amount;
            $case->dept_currency   = $request->dept_currency;
            $case->payable_dept    = $payable_dept;
            $case->discount        = $percentChange;
            if ($request->due_at !== null) {
                $case->due_at = date('Y-m-d', strtotime('01-' . $request->due_at));
            }
            $case->status = CaseStatus::Open;

            $case->save();

            if ($request->pb) {
                foreach ($request->pb as $buyer) {
                    $this->savePossibleBuyer((object)$buyer, $case->id);
                }
            }

            $url = route('watermark', ['case_no' => $case_no]);

            $pdf = \PDF::loadView('pdf.agreement', compact('user', 'url'));

            \Storage::disk('files')->put($path, $pdf->output());

            //todo : sözleşme ile ilgili mail atılacak

            \Session::flash('info', __('labels.inside.add_case'));

            return redirect()->route('inside.index');
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {
                $case = DeptCase::findOrFail($id);
                if ($case->user_id !== \Auth::user()->id) {
                    return redirect()->route('inside.index');
                }
                $tax_offices = TaxOffice::pluck('office', 'id');
                $cities      = City::pluck('name', 'name');
                $invites     = Invite::where('case_id', $id)->get();

                return view('user.edit-case', compact('case', 'tax_offices', 'cities', 'invites'));
            } catch (\Exception $exception) {
                return redirect()->route('inside.create.index');
            }
        }

        /**
         * Update case
         *
         * @param CaseEditRequest $request
         * @param                 $id
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function update(CaseEditRequest $request, $id)
        {
            try {

                $case = DeptCase::findOrFail($id);

                if ($case->user_id !== \Auth::user()->id) {
                    return redirect()->route('inside.index');
                }

                $user          = \Auth::user();
                $dept_amount   = str_replace(',', '', $request->dept_amount);
                $payable_dept  = str_replace(',', '', $request->payable_dept);
                $percentChange = $this->percentage($dept_amount, $payable_dept);
                $path          = $user->id . DIRECTORY_SEPARATOR . $case->case_no . '.pdf';

                $case->dept_amount   = $dept_amount;
                $case->dept_currency = $request->dept_currency;
                $case->payable_dept  = $payable_dept;
                $case->discount      = $percentChange;
                if ($request->due_at) {
                    $case->due_at = date('Y-m-d', strtotime('01-' . $request->due_at));
                } else {
                    $case->due_at = null;
                }
                $case->status = CaseStatus::Open;

                $case->save();

                if ($request->pb) {
                    //clear invites
                    Invite::where('case_id', $case->id)->delete();

                    //recreate invites
                    foreach ($request->pb as $buyer) {
                        $this->savePossibleBuyer((object)$buyer, $case->id);
                    }
                }

                $url = route('watermark', ['case_no' => $case->case_no]);

                $pdf = \PDF::loadView('pdf.agreement', compact('user', 'url'));

                \Storage::disk('files')->put($path, $pdf->output());

                \Session::flash('info', __('labels.inside.add_case'));

                return redirect()->route('inside.index');

            } catch (\Exception $ex) {
                return redirect()->route('inside.index');
            }
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return array
         * @throws \Exception
         */
        public function destroy($id)
        {
            try {
                $case = DeptCase::findOrFail($id);

                if ($case->user_id !== \Auth::user()->id) {
                    return ['status' => false, 'message' => '403'];
                }

                $case->delete();

                \Session::flash('info', __('labels.inside.update_info'));

                return ['status' => true];
            } catch (\Exception $ex) {
                return ['status' => false, 'message' => '404'];
            }
        }


        public function addByProxy()
        {
            $tax_offices = TaxOffice::pluck('office', 'id');
            $cities      = City::pluck('name', 'name');
            $user        = \Auth::user();

            return view('user.add-by-proxy', compact('tax_offices', 'cities', 'user'));
        }

        /**
         * Chances case status
         *
         * @param Request $request
         *
         * @return array
         */
        public function caseStatus(Request $request)
        {
            try {
                $case = DeptCase::findOrFail($request->id);

                if ($case->user_id !== \Auth::user()->id) {
                    return ['status' => false, 'message' => '403'];
                }

                $case->status = $case->status === 1 ? CaseStatus::Suspended : CaseStatus::Open;
                $case->save();

                \Session::flash('info', __('labels.inside.update_info'));

                return ['status' => true];
            } catch (\Exception $ex) {
                return ['status' => false, 'message' => '404'];
            }
        }

        /***
         * Get profile info
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function profile()
        {
            $user        = \Auth::user();
            $tax_offices = TaxOffice::pluck('office', 'id');

            return view('user.profile', compact('user', 'tax_offices'));
        }

        /**
         * Update profile info
         *
         * @param SellerRequest $request
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function updateProfile(SellerRequest $request)
        {
            $user           = User::findOrFail($request->id);
            $user->password = \Hash::make($request->password);
            $user->save();

            \Session::flash('info', __('labels.inside.update_info'));

            return redirect()->route('inside.index');
        }

        /**
         * Show contract by case no
         *
         * @param $path
         *
         * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
         */
        public function showContract($path)
        {
            $id = \Auth::user()->id;

            try {

                DeptCase::where('user_id', $id)->where('case_no', $path)->firstOrFail();

                $file = $id . DIRECTORY_SEPARATOR . $path . '.pdf';
                if (!\Storage::disk('files')->exists($file)) {
                    return __('labels.inside.file_not_found');
                }

                return response()->download(
                  \Storage::disk('files')->path($file),
                  null,
                  [],
                  null);

            } catch (ModelNotFoundException $exception) {
                return __('labels.inside.file_not_found');
            }
        }

        /**
         * Search page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function search()
        {
            $cities = City::all()->pluck('name', 'name');

            return view('user.search', compact('cities'));
        }

        /**
         * Search perform
         *
         * @param SearchRequest $request
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function doSearch(SearchRequest $request)
        {
            $param = $request->tax_number;
            $cases = DeptCase::where('tax_no', $param)->where('user_id', '!=', auth()->user()->id)->get(['title']);

            $search_log            = new SearchLog();
            $search_log->user_id   = auth()->user()->id;
            $search_log->tax_no    = $request->tax_number;
            $search_log->title     = $request->title;
            $search_log->has_found = $cases->count() > 0 ? 1 : 0;
            $search_log->save();

            $cities = City::all()->pluck('name', 'name');

            return view('user.search', compact('param', 'cases', 'cities'));

        }

        /**
         * Save possible seller after search has no data
         *
         * @param PossibleSellerRequest $request
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function savePossibleSeller(PossibleSellerRequest $request)
        {
            $i = Invite::where('tax_no', $request->tax_no)->first();

            $phone = preg_replace('/\D/', '', $request->phone);

            if (!$i) {
                $invite          = new Invite();
                $invite->is_main = 1;
            } else {
                $invite          = new Invite();
                $invite->is_main = 0;
            }

            $invite              = new Invite();
            $invite->user_id     = \Auth::user()->id;
            $invite->title       = $request->title;
            $invite->tax_no      = $request->tax_no;
            $invite->email       = $request->email;
            $invite->city        = $request->city;
            $invite->responsible = $request->responsible;
            $invite->phone       = $phone;
            $invite->type        = InviteType::SELLER;

            $invite->save();

            return redirect()->route('inside.index')->with('info', __('labels.inside.update_info'));
        }

        /**
         * User reminder
         *
         * @param Request $request
         *
         * @return array
         */
        public function saveInform(Request $request)
        {
            $param = $request->keyword;

            $reminder          = new Reminder();
            $reminder->user_id = \Auth::user()->id;
            $reminder->keyword = $param;

            $reminder->save();

            \Session::flash('info', __('labels.inside.update_info'));

            return ['status' => true];
        }

        /**
         * Watermark
         *
         * @param $case_no
         *
         * @return mixed
         */
        public function watermark($case_no)
        {
            //WWW.PARAONAY.COM VE “XXXXXXXX TARİH VE SERİ NUMRALI SÖZLEŞME YAZACAK

            $img = \Image::make(storage_path('app/a4-trans.png'));

            // use callback to define details
            $img->text(
              'WWW.PARAONAY.COM VE ' . $case_no . ' TARİH VE SERİ NUMRALI SÖZLEŞME', 250, 300, function ($font) {
                $font->file(storage_path() . '/fonts/Roboto-Regular.ttf');
                $font->size(20);
                $font->color('#ff0000');
                $font->align('center');
                $font->valign('center');
                $font->angle(45);
            });

            return $img->response('png');
        }

        /**
         * Save possible buyer with dept case
         *
         * @param object $buyer
         * @param        $case_id
         */
        private function savePossibleBuyer($buyer, $case_id)
        {

            $i = Invite::where('tax_no', $buyer->tax_no)->first();

            $phone = preg_replace('/\D/', '', $buyer->phone);

            if (!$i) {
                $invite          = new Invite();
                $invite->is_main = 1;
            } else {
                $invite          = new Invite();
                $invite->is_main = 0;
            }

            $invite->user_id     = \Auth::user()->id;
            $invite->case_id     = $case_id;
            $invite->email       = $buyer->email;
            $invite->title       = $buyer->title;
            $invite->tax_no      = $buyer->tax_no;
            $invite->city        = $buyer->city;
            $invite->responsible = $buyer->responsible;
            $invite->phone       = $phone;
            $invite->type        = InviteType::BUYER;

            //todo : mail girildiyse eklenene mail at
            //todo : notify admin

            $invite->save();
        }

        /***
         * Case name helper
         * @return string
         * @throws \Exception
         */
        private function caseName(): string
        {
            //todo : random 2 harf al
            //$harfler = ['A'...'Z'];
            return mb_substr(\Auth::user()->name, 0, 1, 'utf-8') . mb_substr(\Auth::user()->surname, 0, 1, 'utf-8') . '-' . random_int(1000000, 9999999) . '-' . date('Y');
        }

        /**
         * Calculates percentage
         *
         * @param $dept
         * @param $payable_dept
         *
         * @return float|int
         */
        private function percentage($dept, $payable_dept)
        {
            $diff = $dept - $payable_dept;
            $diff = abs($diff);

            return ($diff / $dept) * 100;
        }
    }
