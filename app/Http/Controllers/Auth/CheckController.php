<?php

namespace App\Http\Controllers\Auth;

use App\Enums\LoginType;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckSmsRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class CheckController extends Controller
{

    /**
     * Kullanıcı sms kontrol
     * @param CheckSmsRequest $checkSmsRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkSms(CheckSmsRequest $checkSmsRequest)
    {
        try {

            $user = User::findOrFail($checkSmsRequest->uid);

            if ((int)$user->sms === (int)$checkSmsRequest->sms) {

                $user->sms_approved = 1;
                $user->save();

                return redirect()->route('email.info');

            }

            \Session::flash('user', $user);

            return redirect()->back()->withErrors(['sms_error' => true]);


        } catch (ModelNotFoundException $exception) {
            return redirect()->route('register');
        }
    }

    /**
     * Kullanıcı email kontrol
     * @param $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkEmail($hash)
    {

        try {

            $user                 = User::where('email_hash', $hash)->firstOrFail();
            $user->email_approved = 1;
            $user->save();

            return redirect()->route('login');

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('register');
        }

    }

    public function emailInfo()
    {
        return view('auth.email-info');
    }


    public function linkedInLogin()
    {
        return \Socialite::driver('linkedin')->redirect();
    }

    public function linkedInCallback()
    {
        $data = \Socialite::driver('linkedin')->fields(
            [
                'id', 'first-name', 'last-name', 'email-address',
                'headline', 'location', 'industry', 'positions',
                'num-connections', 'num-connections-capped',
                'summary', 'specialties', 'public-profile-url'
            ]
        )->stateless()->user();

        $authUser = $this->findOrCreateUser($data);
        \Auth::login($authUser, true);

        return redirect()->route('email.info');

        /**
         * +token: "AQUpo8G7K6gYjT8YDRnJAqKwVloiQK1ZzqP2rXyWdE8nb5E-Ek1EPY7REvHb6FDWR8-A9iRcLyeDhmS3o8hvH8AZp-9KPQrzJaz4o72SvbqiYArAoBhDHxjBJNgB4Ymsr_UJPtIPz2szUwS7e-wzqm0btmz9pE8c ▶"
        +refreshToken: null
        +expiresIn: 5183999
        +id: "D_CK65rxZA"
        +nickname: null
        +name: null
        +email: "rhnkyr@yahoo.com"
        +avatar: null
        +user: array:11 [▼
        "emailAddress" => "rhnkyr@yahoo.com"
        "firstName" => "Erhan"
        "headline" => "Co-founder Lim (l.im) & erhpozitif.com"
        "id" => "D_CK65rxZA"
        "industry" => "Computer Software"
        "lastName" => "Kayar"
        "location" => array:2 [▼
        "country" => array:1 [▶]
        "name" => "Chiang Mai, Thailand"
        ]
        "numConnections" => 500
        "numConnectionsCapped" => true
        "positions" => array:2 [▼
        "_total" => 1
        "values" => array:1 [▼
        0 => array:7 [▼
        "company" => array:5 [▼
        "id" => 5384753
        "industry" => "Internet"
        "name" => "Lim"
        "size" => "2-10"
        "type" => "Public Company"
        ]
        "id" => 599953281
        "isCurrent" => true
        "location" => array:1 [▼
        "name" => "Antalya, Turkey"
        ]
        "startDate" => array:2 [▼
        "month" => 9
        "year" => 2014
        ]
        "summary" => "CTO"
        "title" => "Co Founder"
        ]
        ]
        ]
        "publicProfileUrl" => "https://www.linkedin.com/in/erhan-kayar-b4646812"
        ]
        +"avatar_original": null
         */
    }

    public function findOrCreateUser($data)
    {
        $authUser = User::where('email', $data->email)->first();
        if ($authUser) {
            return $authUser;
        }

        return User::create([
            'name'            => $data->user['firstName'],
            'surname'         => $data->user['lastName'],
            'email'           => $data->email,
            'want_connection' => 1,
            'email_hash'      => str_limit(md5($data->email . str_random()), 25, ''),
            'email_approved'  => 0,
            'sms_approved'    => 0,
            'ip'              => request()->getClientIp(),
            'login_type'      => LoginType::LINKEDIN
        ]);

    }
}