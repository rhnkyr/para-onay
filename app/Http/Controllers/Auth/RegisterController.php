<?php

namespace App\Http\Controllers\Auth;

use App\Enums\LoginType;
use App\Enums\UserType;
use App\Models\TaxOffice;
use App\Models\User;
use App\Http\Controllers\Controller;
use Epigra\TcKimlik;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register-complete';


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function showRegistrationForm()
    {
        $tax_offices = TaxOffice::pluck('office', 'office');
        return view('auth.register', compact('tax_offices'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = \Validator::make($data, [
            'name'                 => 'required|string|max:190',
            'surname'              => 'required|string|max:190',
            'birth_year'           => 'required|digits:4',
            'tc_no'                => 'required|digits:11|unique:users',
            'title_of_company'     => 'required|string|max:190',
            'company_tax_no'       => 'required|digits:10|unique:users',
            'tax_office_name'      => 'required|string|max:190',
            'gsm'                  => 'required|string|max:190',
            'email'                => 'required|string|email|max:190|unique:users',
            //'password'             => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        /*$validator->after(function ($validator) use ($data) {
            if (!TcKimlik::verify($data['tc_no'])) {
                $validator->errors()->add('tc_no', 'Geçersiz kimlik numarası');
            }
        });*/

        $validator->after(function ($validator) use ($data) {

            $data = [
                'tcno'      => $data['tc_no'],
                'isim'      => $data['name'],
                'soyisim'   => $data['surname'],
                'dogumyili' => $data['birth_year'],
            ];

            if (!TcKimlik::validate($data)) {
                $validator->errors()->add('tc_no', 'Geçersiz kimlik bilgileri');
            }
        });

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        //todo : burası dinamik olacak
        $approved  = 1;
        $key       = random_int(100000, 999999);
        $mail_hash = str_limit(md5($data['email'] . str_random()), 25, '');
        $office    = TaxOffice::findOrFail($data['tax_office_name']);
        $phone     = preg_replace('/\D/', '', $data['gsm']);

        $user = User::create([
            'name'             => $data['name'],
            'surname'          => $data['surname'],
            'tc_no'            => $data['tc_no'],
            'birth_year'       => $data['birth_year'],
            'title_of_company' => $data['title_of_company'],
            'company_tax_no'   => $data['company_tax_no'],
            'tax_office_name'  => $data['tax_office_name'],
            'gsm'              => $phone,
            'city_id'          => $office->plate,
            'email'            => strtolower(trim($data['email'])),
            'want_connection'  => $data['want_connection'] ?? 1,
            'password'         => \Hash::make($key),
            'sms'              => $key,
            'email_hash'       => $mail_hash,
            'email_approved'   => $approved,
            'sms_approved'     => $approved,
            'ip'               => request()->getClientIp(),
            'proxy'            => $data['proxy'],
            'login_type'       => LoginType::EMAIL
        ]);

        $user->assignRole('user');

        //tüm mailer ve sms işlemleri UserObserver içinde

        \Session::flash('user', $user);

        return $user;
    }

    protected function registerComplete()
    {
        if (!\Session::has('user')) {
            return redirect()->route('register');
        }

        $user = \Session::get('user');

        return view('auth.sms', compact('user'));
    }
}
