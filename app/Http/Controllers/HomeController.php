<?php

namespace App\Http\Controllers;

use App\Models\Creditor;
use Epigra\TcKimlik;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$check = TcKimlik::verify('25954640690'); //string
        //var_dump($check);

        /*Role::create(['name' => 'admin']);
        $user = \Auth::user();
        $user->assignRole('admin');*/

        /*$user = \Auth::user();
        if ($user->hasRole('admin'))
            return view('home');

        return ':|';*/

        /*$creditor          = new Creditor();
        $creditor->user_id = \Auth::user()->id;
        $creditor->save();*/

        $user_name = 'Erhan Kayar';
        //$pdf = \PDF::loadView('pdf.agreement',compact('user_name'));
        //return $pdf->download('invoice.pdf');

        $pdf = \PDF::loadView('pdf.agreement', compact('user_name'));
        return $pdf->stream('document.pdf');

        //return view('home');

    }
}
