<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReminderEmail extends Mailable
{
    use Queueable, SerializesModels;


    private $name_surname;

    /**
     * Create a new message instance.
     *
     * @param $name_surname
     */
    public function __construct($name_surname)
    {
        $this->name_surname = $name_surname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name_surname = $this->name_surname;
        return $this->subject('Para Onay Email Doğrulama')->view('emails.reminder', compact('name_surname'));
    }
}
