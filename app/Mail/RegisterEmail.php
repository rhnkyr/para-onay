<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterEmail extends Mailable
{
    use Queueable, SerializesModels;


    private $name_surname;
    private $mail_hash;

    /**
     * Create a new message instance.
     *
     *
     * @param $name_surname
     * @param $mail_hash
     */
    public function __construct($name_surname, $mail_hash)
    {
        $this->name_surname = $name_surname;
        $this->mail_hash    = $mail_hash;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name_surname = $this->name_surname;
        $mail_hash    = $this->mail_hash;
        return $this->subject('Para Onay Email Doğrulama')->view('emails.register', compact('name_surname', 'mail_hash'));
    }
}
