<?php
/**
 * Created by PhpStorm.
 * User: erhankayar
 * Date: 20.05.2018
 * Time: 22:59
 */

namespace App\Observers;

use App\Jobs\CheckReminderJob;
use App\Jobs\RegisterEmailJob;
use App\Jobs\SendSmsJob;
use App\Models\User;

class UserObserver
{
    /**
     * Kullanıcı yaratıldığında çalışacak kısım
     * @param User $user
     */
    public function created(User $user)
    {
        //todo : production da hepsi açılacak
        //$job = (new SendSmsJob($user->gsm, $user->sms))->onQueue('sms');
        //dispatch($job);

        //$job = (new RegisterEmailJob($user->full_name, $user->email, $user->email_hash))->onQueue('emails');
        //dispatch($job);

        //$job = (new CheckReminderJob($user))->onQueue('emails');
        //dispatch($job);

    }

    /**
     * Kullanıcı silinirken yaratılacak kısım
     *
     * @param User $user
     * @return void
     */
    public function deleting(User $user)
    {
        //
    }
}