<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('surname');
            $table->integer('birth_year');
            $table->string('title_of_user');
            $table->string('industry');
            $table->string('num_connections');
            $table->string('public_profile_url');
            $table->string('tc_no', 11)->unique();
            $table->string('title_of_company');
            $table->string('company_tax_no')->unique();
            $table->string('tax_office_name');
            $table->string('email')->unique();
            $table->string('gsm');
            $table->unsignedInteger('city_id');
            $table->string('password');
            $table->string('ip');
            $table->integer('sms');
            $table->tinyInteger('login_type');
            $table->tinyInteger('type')->comment = '1- Satıcı, 2- Alıcı';
            $table->string('email_hash');
            $table->tinyInteger('sms_approved');
            $table->tinyInteger('email_approved');
            $table->unsignedInteger('proxy')->default(0)->comment               = 'Temsilci ise ne tür bi temsilci (Avukat vb)';
            $table->unsignedTinyInteger('want_connection')->default(1)->comment = '0 - İletişime geçme. 1 - İletişime geçebilir (Eğer iletişime geçilmesini istiyorsa)';
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
