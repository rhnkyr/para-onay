<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('user_id');
            $table->uuid('case_id')->nullable();
            $table->string('title');
            $table->string('tax_no')->nullable();
            $table->string('email')->nullable();
            $table->string('responsible')->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->integer('type');
            $table->integer('is_main');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invites');
    }
}
