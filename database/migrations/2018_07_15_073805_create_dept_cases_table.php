<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeptCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dept_cases', function (Blueprint $table) {
            $table->uuid('id')->index();//case_identifier
            $table->uuid('user_id')->index();
            $table->string('case_no')->index();
            $table->string('title')->index();
            $table->string('tax_no')->index();
            $table->string('tax_office_name');
            $table->string('city_id');
            $table->decimal('dept_amount', 10, 2);
            $table->string('dept_currency');
            $table->decimal('payable_dept', 10, 2);
            $table->decimal('discount', 10, 2);
            $table->date('due_at')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->timestamp('closed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deptcase');
    }
}
