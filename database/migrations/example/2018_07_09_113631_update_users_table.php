<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname');
            $table->integer('birth_year');
            $table->string('title_of_user');
            $table->string('industry');
            $table->string('num_connections');
            $table->string('public_profile_url');
            $table->tinyInteger('type')->comment = '1- Satıcı, 2- Alıcı';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname');
            $table->integer('birth_year');
            $table->string('title_of_user');
            $table->string('industry');
            $table->string('num_connections');
            $table->string('public_profile_url');
            $table->tinyInteger('type')->comment = '1- Satıcı, 2- Alıcı';
        });
    }
}
