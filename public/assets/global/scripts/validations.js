var FormValidation = function () {

    // basic validation
    var handleValidation = function() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation
        $.extend( $.validator.messages, {
            required: "Bu alanın doldurulması zorunludur.",
            remote: "Lütfen bu alanı düzeltin.",
            email: "Lütfen geçerli bir e-posta adresi giriniz.",
            url: "Lütfen geçerli bir web adresi (URL) giriniz.",
            date: "Lütfen geçerli bir tarih giriniz.",
            dateISO: "Lütfen geçerli bir tarih giriniz(ISO formatında)",
            number: "Lütfen geçerli bir sayı giriniz.",
            digits: "Lütfen sadece sayısal karakterler giriniz.",
            creditcard: "Lütfen geçerli bir kredi kartı giriniz.",
            equalTo: "Lütfen aynı değeri tekrar giriniz.",
            extension: "Lütfen geçerli uzantıya sahip bir değer giriniz.",
            maxlength: $.validator.format( "Lütfen en fazla {0} karakter uzunluğunda bir değer giriniz." ),
            minlength: $.validator.format( "Lütfen en az {0} karakter uzunluğunda bir değer giriniz." ),
            rangelength: $.validator.format( "Lütfen en az {0} ve en fazla {1} uzunluğunda bir değer giriniz." ),
            range: $.validator.format( "Lütfen {0} ile {1} arasında bir değer giriniz." ),
            max: $.validator.format( "Lütfen {0} değerine eşit ya da daha küçük bir değer giriniz." ),
            min: $.validator.format( "Lütfen {0} değerine eşit ya da daha büyük bir değer giriniz." ),
            require_from_group: $.validator.format( "Lütfen bu alanların en az {0} tanesini doldurunuz." )
        } );


        $("#login").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                email: "required",
                password: "required"
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        $("#addCase").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                title: "required",
                tax_no: "required",
                tax_office_name: "required",
                dept_amount: "required",
                dept_currency: "required",
                payable_dept: "required",
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        $("#editCase").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                dept_amount: "required",
                dept_currency: "required",
                payable_dept: "required",
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        $("#addPossibleSeller").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                title: "required",
                tax_no: "required",
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        $("#search").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                param: {
                    required: true,
                    digits: true,
                    maxlength: 10,
                    minlength: 10

                },
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            messages : {
                param : {
                    maxlength: $.validator.format("Lütfen {0} basamaklı bir değer giriniz."),
                    minlength: $.validator.format("Lütfen {0} basamaklı bir değer giriniz.")
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleValidation();
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});
